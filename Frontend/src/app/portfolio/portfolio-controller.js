(function() {
  'use strict';

  angular
    .module('project')
    .controller('PortfolioController', PortfolioController);

  function PortfolioController($state, $scope) {
    $(document).ready(function(){
      $(".box-shadow, .small-box-shadow").mouseenter(function(){
        $(this).fadeTo(600, 1)
      });
      $(".box-shadow, .small-box-shadow").mouseleave(function(){
        $(this).fadeTo(600, 0.5)
      });
    });    
  };
})();