(function() {
  'use strict';

  angular
    .module('project')
    .controller('SectionsController', SectionsController);

  function SectionsController($state, $scope) {
    $(document).ready(function(){
      $(".box-shadow, .small-box-shadow").mouseenter(function(){
        $(this).fadeTo(600, 1)
      });
      $(".box-shadow, .small-box-shadow").mouseleave(function(){
        $(this).fadeTo(600, 0.5)
      });
      $(".red-bye").show(1500);
      $(".rotate1").textrotator({
        animation: "flipUp", //fade, flip, flipUp, flipCube, flipCubeUp and spin.
        separator: "/", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
        speed: 2500 // How many milliseconds until the next word show.
      });
      $('#dev').mouseenter(function(){
        $(this).hide(800)
        $('#design').show(800)
      });
      $('#design').mouseleave(function(){
        $('#dev').show(800);
        $('#design').hide(800);
      });
    });    
  };
})();