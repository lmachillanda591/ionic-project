(function() {
  'use strict';

  angular
    .module('project')
    .controller('ContactsController', ContactsController);

  function ContactsController($state, $scope, ContactsFactory, toastr) {
    $scope.GetUser = GetUser;
    function GetUser () {
      ContactsFactory.access(function () {
        console.log(".");
      });
    }

    $scope.Logout = function () {
      window.localStorage.removeItem('session');
      window.localStorage.removeItem('id');
      toastr.info('Sesión cerrada', '¡Nos vemos pronto!');    
    }

    $scope.IsConnected = function () {
      if (window.localStorage.getItem('session')) {
        return true
      }
      else {
        return false
      }
    }
  };
})();