(function() {
  'use strict';

  angular
    .module('project')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home2', {
        url: '/home_en',
        templateUrl: 'app/main/main_en.html',
        controller: 'MainController',
        controllerAs: 'home'
      })
      .state('home1', {
        url: '/home_es',
        templateUrl: 'app/main/main_es.html',
        controller: 'MainController',
        controllerAs: 'home'
      })
      .state('register',{
        url: '/register',
        templateUrl: 'app/register/register.html',
        controller: 'RegisterController',
        controllerAs: 'register'
      })
      .state('session',{
        url: '/session',
        templateUrl: 'app/session/session.html',
        controller: 'SessionController',
        controllerAs: 'session'
      })
      .state('contacts',{
        url: '/contacts',
        templateUrl: 'app/contacts/contacts.html',
        controller: 'ContactsController',
        controllerAs: 'contacts'
      })
      .state('portfolio',{
        url: '/portfolio',
        templateUrl: 'app/portfolio/portfolio.html',
        controller: 'PortfolioController',
        controllerAs: 'portfolio'
      })
      .state('web-section',{
        url: '/web-section',
        templateUrl: 'app/sections/web/web.html',
        controller: 'SectionsController',
        controllerAs: 'web'
      });

    $urlRouterProvider.otherwise('/home_es');
  }
})();