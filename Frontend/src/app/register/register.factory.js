'use strict';

angular
  .module('project')
  .factory('RegisterFactory', function ($http, $window, $state, URL, toastr) {

    return {
      access: function (user, callback) {
        
        $http({
          method: 'post',
          url: URL+'user',
          data: user
        })
        .success(function (data) {        
          if (callback) callback(data);
          console.log(data);
          $state.go('session');
          toastr.success('Registro hecho satisfactoriamente', '¡Bien Hecho '+data.name+'!');
        }); 
      }
    }
  });