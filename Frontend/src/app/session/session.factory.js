'use strict';

angular
  .module('project')
  .factory('SessionFactory', function ($http, $window, $state, URL, $rootScope, $stateParams) {

    return {
      access: function (session, callback) {
        
        $http({
          method: 'post',
          url: URL+'session',
          data: session
        })
        .success(function (data) {        
          if (callback) callback(data);
          window.localStorage.setItem('session', data.session.token);
          var id = data.user.id * (1993*1993*1993*1993*1993*1993*1993) / 12
          window.localStorage.setItem('id', id);     
          $state.go('contacts');
        }); 
      }
    }
  });