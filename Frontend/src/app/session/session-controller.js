(function() {
  'use strict';

  angular
    .module('project')
    .controller('SessionController', SessionController);

  function SessionController($state, SessionFactory, $scope) {
    $scope.session = {};
    $scope.Access = Access;

    function Access () {
      SessionFactory.access($scope.session, function () {
        console.log(".");
      });
    }
  };
})();