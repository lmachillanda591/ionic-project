(function() {
  'use strict';

  angular
    .module('project')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $scope, $anchorScroll, $location) {

    var vm= this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1456451119842;
    vm.showToastr = showToastr;

    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }

    $scope.Logout = function () {
      window.localStorage.removeItem('session');
      window.localStorage.removeItem('id');
      toastr.info('Sesión cerrada', '¡Nos vemos pronto!');  
    }

    $scope.isConnected = function () {
      if (window.localStorage.getItem('session')){
        return true
      }
      else {
        return false
        }
      };

    //scrollTo ID in the HTML
    $(document).ready(function(){
      $('.scrollTo').click(function(e){
        e.preventDefault();
        $('html, body').stop().animate({scrollTop: $($(this).attr('href')).offset().top}, 500);
      });
      $('#dropdown-toggle-service').click(function(){
        $('#dropdown-menu-service').show(400)
        $('#dropdown-toggle-service').click(function(){
          $('#dropdown-menu-service').hide(400)
        });
      });
      $('#dropdown-toggle-service').hover(function(){
        $('#dropdown-menu-service').show(400)
        $('#dropdown-menu-service').mouseleave(function(){
          $(this).hide(400)
        });
      });

      $('#dropdown-toggle-we').click(function(){
        $('#dropdown-menu-we').show(400)
        $('#dropdown-menu-we').mouseleave(function(){
          $(this).hide(400)
        });
      });

      $('#dropdown-toggle-we').hover(function(){
        $('#dropdown-menu-we').show(400)
        $('#dropdown-menu-we').mouseleave(function(){
          $(this).hide(400)
        });
      });
    });
  }
})();